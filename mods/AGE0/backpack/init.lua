local S = minetest.get_translator("backpack")

local function getBackpackSize(stack)
	if not stack:get_definition().groups.backpack_size then
		return 0
	end

	return stack:get_definition().groups.backpack_size
end

local function openBackpack(player, player_name, backapack_stack, context--[[opened equipped or inhand]])
	--minetest.chat_send_all("Enter")
	local inv_name = player_name .. "_backpack_tmp"
	--Chaeck if something is broken. To prevent item voiding
	local inv = minetest.get_inventory({type="detached", name=inv_name})
	if inv ~= nil then
		return false, "Backpack is already open!"
	end
	
	minetest.create_detached_inventory(player_name .. "_backpack_tmp", {
		--Forbids puting backpacks into backpacks
		allow_put = function(inv, listname, index, stack, player)
			if minetest.get_item_group(stack:get_name(), "backpack_size") > 0  then
				return 0
			else
				return stack:get_stack_max()
			end
		end,
	}, player_name)
	inv = minetest.get_inventory({type = "detached", name=inv_name})
	inv:set_size("main", getBackpackSize(backapack_stack) * 9)

	local meta = backapack_stack:get_meta()
	--minetest.chat_send_all(dump(meta:to_table()))
	local content = meta:get_string("backpack:content")
	local list = content and minetest.deserialize(content) or {}

	for i,stack in ipairs(list) do
		inv:set_stack("main", i, stack)
		--[[if stack ~= nil then
			minetest.chat_send_all(dump(stack:to_string()))
		end]]
	end

	local form = "size[9," .. (getBackpackSize(backapack_stack) + 6) .. "]"
		.. mcl_formspec.get_itemslot_bg(0, 0, 9, getBackpackSize(backapack_stack))
		.. "list[detached:".. inv_name .. ";main;0,0;9," .. getBackpackSize(backapack_stack) .. ";]"
		.. mcl_formspec.get_itemslot_bg(0, getBackpackSize(backapack_stack) + 1.5, 9, 3)
		.. "list[current_player;main;0," .. (getBackpackSize(backapack_stack) + 1.5) .. ";9,3;9]"
		.. mcl_formspec.get_itemslot_bg(0, getBackpackSize(backapack_stack) + 5, 9, 1)
		.. "list[current_player;main;0," .. (getBackpackSize(backapack_stack) + 5) .. ";9,1;]"
		.. "listring[current_player;main]"
		.. "listring[detached:".. inv_name .. ";main]"
		.. "listring[current_player;main]"
	minetest.show_formspec(player_name, "backpack:" .. context, form)
end

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname == "backpack:equipped" and fields.quit then
		--minetest.chat_send_all("Exit")
		local player_inv = player:get_inventory()
		local backapack_stack = player_inv:get_stack("backpack", 1)
		
		local inv_name = player:get_player_name() .. "_backpack_tmp"
		local inv = minetest.get_inventory({type="detached", name=inv_name})
		local stacks = {}
		for i = 0, inv:get_size("main"), 1 do
			stacks[i] = inv:get_stack("main", i):to_string()
		end
		
		backapack_stack:get_meta():set_string("backpack:content", minetest.serialize(stacks))
		player_inv:set_stack("backpack", 1, backapack_stack)
		--minetest.chat_send_all(dump(meta:to_table()))
		minetest.remove_detached_inventory(inv_name)
	elseif formname == "backpack:inhand" and fields.quit then
		local backapack_stack = player:get_wielded_item()

		local inv_name = player:get_player_name() .. "_backpack_tmp"
		local inv = minetest.get_inventory({type="detached", name=inv_name})
		local stacks = {}
		for i = 0, inv:get_size("main"), 1 do
			stacks[i] = inv:get_stack("main", i):to_string()
		end

		backapack_stack:get_meta():set_string("backpack:content", minetest.serialize(stacks))
		player:set_wielded_item(backapack_stack)
		minetest.remove_detached_inventory(inv_name)
	else
        return
    end
	
end)

--A chat command for now. Wonna change to hot-key later.
minetest.register_chatcommand("bag", {
    func = function(player_name)
		local player = minetest.get_player_by_name(player_name)
		local player_inv = player:get_inventory()
		local backapack_stack = player_inv:get_stack("backpack", 1)
		if backapack_stack:is_empty() then
			return false, "No backpack equiped!"
		end
		
		openBackpack(player, player_name, backapack_stack, "equipped")
		return true, ""
    end,
})

local function on_backpack_place(itemstack, user, pointed_thing)
	openBackpack(user, user:get_player_name(), itemstack, "inhand")
	return nil
end

minetest.register_allow_player_inventory_action(function(player, action, inventory, inventory_info)
	if action == "move" and inventory_info.to_list == "backpack" then
		local itemstack = inventory:get_stack(inventory_info.from_list, inventory_info.from_index)
		if not (minetest.get_item_group(itemstack:get_name(), "backpack_size") > 0)  then
			return 0
		else
			return itemstack:get_stack_max()
		end
	end
end)

-- register backapack's
minetest.register_tool("backpack:leather", {
	description = S("Leather backpack"),
	inventory_image = "backpack_leather.png",
	groups = {backpack_size = 1},
	on_place = on_backpack_place,
	on_secondary_use = on_backpack_place,
})

minetest.register_craftitem("backpack:iron", {
	description = S("Iron backpack"),
	inventory_image = "backpack_iron.png",
	groups = {backpack_size = 2},
	on_place = on_backpack_place,
	on_secondary_use = on_backpack_place,
})

minetest.register_craftitem("backpack:gold", {
	description = S("Gold backpack"),
	inventory_image = "backpack_gold.png",
	groups = {backpack_size = 3},
	on_place = on_backpack_place,
	on_secondary_use = on_backpack_place,
})

minetest.register_craftitem("backpack:diamond", {
	description = S("Diamond backpack"),
	inventory_image = "backpack_diamond.png",
	groups = {backpack_size = 4},
	on_place = on_backpack_place,
	on_secondary_use = on_backpack_place,
})

-- register bag crafts
minetest.register_craft({
	output = "backpack:leather",
	recipe = {
		{"mcl_mobitems:leather", "mcl_mobitems:leather", "mcl_mobitems:leather"},
		{"mcl_mobitems:leather", "mcl_chests:chest", "mcl_mobitems:leather"},
		{"mcl_mobitems:leather", "mcl_mobitems:leather", "mcl_mobitems:leather"}
	}
})

minetest.register_craft({
	output = "backpack:iron",
	recipe = {
		{"mcl_core:iron_ingot", "mcl_core:iron_ingot", "mcl_core:iron_ingot"},
		{"mcl_core:iron_ingot", "backpack:leather", "mcl_core:iron_ingot"},
		{"mcl_core:iron_ingot", "mcl_core:iron_ingot", "mcl_core:iron_ingot"}
	}
})

minetest.register_craft({
	output = "backpack:gold",
	recipe = {
		{"mcl_core:gold_ingot", "mcl_core:gold_ingot", "mcl_core:gold_ingot"},
		{"mcl_core:gold_ingot", "backpack:iron", "mcl_core:gold_ingot"},
		{"mcl_core:gold_ingot", "mcl_core:gold_ingot", "mcl_core:gold_ingot"}
	}
})

minetest.register_craft({
	output = "backpack:diamond",
	recipe = {
		{"mcl_core:diamond", "mcl_core:diamond", "mcl_core:diamond"},
		{"mcl_core:diamond", "backpack:gold", "mcl_core:diamond"},
		{"mcl_core:diamond", "mcl_core:diamond", "mcl_core:diamond"}
	}
})

--transfer content of backpack on craft
minetest.register_on_craft(function(itemstack, player, old_craft_grid, craft_inv)
	if minetest.get_item_group(itemstack:get_name(), "backpack_size") > 0 then
		content = old_craft_grid[5]:get_meta():get_string("backpack:content")
		itemstack:get_meta():set_string("backpack:content", content)
		return itemstack
	end
end)