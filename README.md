# TechAges
A fork of MineClone2 (An unofficial Minecraft-like game for Minetest).
It aims at implemeting copy of AoE (https://www.curseforge.com/minecraft/modpacks/age-of-engineering) modepack by davqvist.
But in Minetest and with some changes.
All creative work was done by original mod's authors.

### Gameplay
#TODO

#### Gameplay summary
#TODO

## How to play (quick start)
### Getting started
#TODO

### Age 0
#TODO

### Age 1
#TODO

### Additional help
#TODO

### Special items
The following items are interesting for Creative Mode and for adventure
map builders. They can not be obtained in-game or in the creative inventory.

* Barrier: `mcl_core:barrier`

Use the `/giveme` chat command to obtain them. See the in-game help for
an explanation.

## Installation
This game requires [Minetest](http://minetest.net) to run (version 5.4.1 or
later). So you need to install Minetest first. Only stable versions of Minetest
are officially supported.
There is no support for running TechAges in development versions of Minetest.

To install TechAges (if you haven't already), move this directory into the
“games” directory of your Minetest data directory. Consult the help of
Minetest to learn more.

## Useful links
#TODO

## Target
- Create a game based on AoE modepack for Minecraft

## Completion status
This game is currently in **initial** stage.
It is playable, but not yet feature-complete.

The following main features are available:

* None yeat ;)

The following features are incomplete:

* Backpacks
* Drawers
* Tinker's furnace & tools
* Teleportation stones
* Biom finder

## Other readme files

* `LICENSE.txt`: The GPLv3 license text
* `API.md`: For Minetest modders who want to mod this game
* `CREDITS.md`: List of everyone who contributed
